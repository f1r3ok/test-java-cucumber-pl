# Serenity and Cucumber test task by Philip Levin

## Project prerequesities:
* Java Development Kit
* Maven

## To start the project:
* Clone the project, run: git clone https://gitlab.com/f1r3ok/test-java-cucumber-pl.git
* Run: mvn verify

## Fixes:
* Fixed src/test/java/starter/stepdefinitions/SearchStepDefinitions.java - removed useless steps export and fixed the entire steps.
* Fixed src/test/resources/features/search/post_product.feature - fixed incorrect test assertions.
* Removed obslete and useless files and folders - now the project tree is clear.

## CI/CD gitlab pipeline:
* Added .gitlab-ci.yml file which contains build steps and is used to run jobs over CI/CD gitlab pipeline.
